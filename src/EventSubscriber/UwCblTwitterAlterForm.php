<?php

namespace Drupal\uw_cbl_twitter\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblTwitterAlterForm.
 */
class UwCblTwitterAlterForm extends UwCblBase implements EventSubscriberInterface {

  /**
   * Alter form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormAlterEvent $event
   *   The event.
   */
  public function alterForm(FormAlterEvent $event): void {

    // Ensure that we are on a twitter block.
    if ($this->checkLayoutBuilder($event, 'Twitter')) {

      // Get the form from the event.
      $form = &$event->getForm();

      // Add the validation for blockquote.
      $form['#validate'][] = [$this, 'uw_cbl_twitter_validation'];

      // Add the custom form alter to the layout builder.
      $form['settings']['block_form']['#process'][] = [$this, 'uw_cbl_twitter_process_twitter_type'];
    }
  }

  /**
   * Form validation for twitter.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function uw_cbl_twitter_validation(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Get the settings from the form.
    $settings = $form_state->getValue('settings', NULL);

    // If there are settings, continue to process.
    if ($settings) {

      // If there is a block, continue to process.
      if ($block = $settings['block_form']) {

        // Get the type of twitter embed when are going to use.
        $twitter_type = $block['field_uw_tw_feed_type'][0]['value'];

        // Check each of the twitter types and set errors if needed.
        switch ($twitter_type) {

          // The embedded tweet twitter type.
          case 'embed-tweet':

            // Validate the twitter tweet code.
            $this->uw_validate_twitter_parts_and_set_error('tweet', $block['field_uw_tw_list_name'][0]['value'], $form_state);

            break;

          // The collection timeline twitter type.
          case 'timeline':

            // Validate twitter username.
            $this->uw_validate_twitter_parts_and_set_error('username', $block['field_uw_tw_username'][0]['value'], $form_state);

            // Validate twitter URL fragment.
            $this->uw_validate_twitter_parts_and_set_error('url_fragment', $block['field_uw_tw_url_fragment'][0]['value'], $form_state);

            // Validate twitter timeline name.
            $this->uw_validate_twitter_parts_and_set_error('timeline_name', $block['field_uw_tw_timeline_name'][0]['value'], $form_state);

            break;

          // The list twitter type.
          case 'list':

            // Validate the twitter username.
            $this->uw_validate_twitter_parts_and_set_error('username', $block['field_uw_tw_username'][0]['value'], $form_state);

            // Validate the twitter listname.
            $this->uw_validate_twitter_parts_and_set_error('listname', $block['field_uw_tw_list_name'][0]['value'], $form_state);

            break;

          // The likes/profile twitter type.
          case 'faves':
          case 'profile':

            // Validate the twitter username.
            $this->uw_validate_twitter_parts_and_set_error('username', $block['field_uw_tw_username'][0]['value'], $form_state);

            break;

          // The moment twitter type.
          case 'moment':

            // Validate the twitter username.
            $this->uw_validate_twitter_parts_and_set_error('username', $block['field_uw_tw_username'][0]['value'], $form_state);

            // Validate the twitter URL fragment.
            $this->uw_validate_twitter_parts_and_set_error('url_fragment', $block['field_uw_tw_url_fragment'][0]['value'], $form_state);

            break;
        }
      }
    }
  }

  /**
   * Add the process function for states in the twitter type.
   *
   * @param FormStateInterface $form_state
   *   The form state.
   * @return mixed
   *   The element.
   */
  public function uw_cbl_twitter_process_twitter_type(array $element, \Drupal\Core\Form\FormStateInterface $form_state) {

    // Fields to check for.
    $fields = [
      'field_uw_tw_username' => [
        'values' => [
          ['value' => 'faves'],
          ['value' => 'list'],
          ['value' => 'moment'],
          ['value' => 'profile'],
          ['value' => 'timeline'],
        ]
      ],
      'field_uw_tw_tweet_code' => [
        'values' => [
          ['value' => 'embed-tweet'],
        ]
      ],
      'field_uw_tw_list_name' => [
        'values' => [
          ['value' => 'list'],
        ]
      ],
      'field_uw_tw_url_fragment' => [
        'values' => [
          ['value' => 'moment'],
          ['value' => 'timeline'],
        ]
      ],
      'field_uw_tw_timeline_name' => [
        'values' => [
          ['value' => 'timeline'],
        ]
      ],
    ];

    // Step through each field and add the states if they are on the form.
    foreach ($fields as $field => $values) {

      // If the field is in the form add the states.
      if (in_array($field, $element)) {

        // The selector that will determine if visible.
        $selector = 'select[name="settings[block_form][field_uw_tw_feed_type]"]';

        $element[$field]['#states'] = [
          'visible' => [
            $selector => [
              $values['values'],
            ]
          ],
        ];
      }
    }

    return $element;
  }

  /**
   * Validate twitter parts of form.
   *
   * @param string $part
   *   The part of the twitter to validate.
   * @param string $input_to_validate
   *   The actual string inputted to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state object.
   */
  private function uw_validate_twitter_parts_and_set_error(string $twitter_part, string $input_to_validate, \Drupal\Core\Form\FormStateInterface &$form_state): void {

    // The twitter URL fragment validation.
    $twitter_parts['url_fragment'] = [
      'name' => 'URL fragment',
      'reg_expression' => '/^\w{1,15}\/timelines\/\d+$/',
      'field_name' => 'field_uw_tw_url_fragment',
    ];

    // The twitter username validation.
    $twitter_parts['username'] = [
      'name' => 'username',
      'reg_expression' => ' /^[a-zA-Z0-9_]{1,15}$/',
      'field_name' => 'field_uw_tw_username',
    ];

    // The twitter listname validation.
    $twitter_parts['listname'] = [
      'name' => 'listname',
      'reg_expression' => '/^[a-zA-Z][a-zA-Z0-9_-]{0,24}$/',
      'field_name' => 'field_uw_tw_list_name',
    ];

    // The twitter tweet validation.
    $twitter_parts['tweet'] = [
      'name' => 'tweet code',
      'reg_expression' => '/.*twitter\.com\/([^"\/]+\/status\/[0-9]+)([\s\S]+$|)/',
      'field_name' => 'field_uw_tw_tweet_code',
    ];

    // The twitter timeline name validation.
    $twitter_parts['timeline_name'] = [
      'name' => 'timeline name',
      'reg_expression' => '/.*twitter\.com\/([^"\/]+\/status\/[0-9]+)([\s\S]+$|)/',
      'field_name' => 'field_uw_tw_timeline_name',
    ];

    // The twitter timeline name validation.
    $twitter_parts['moment_url'] = [
      'name' => 'URL fragment',
      'reg_expression' => '/^\w{1,15}\/moments\/\d+$/',
      'field_name' => 'field_uw_tw_url_fragment',
    ];

    // Check if the value is null and if so, set error.
    if ($input_to_validate == NULL) {

      // Set the form error for not having a null value.
      $form_state->setErrorByName('settings][block_form][' . $twitter_parts[$twitter_part]['field_name'], t('You must enter a Twitter ' . $twitter_parts[$twitter_part]['name'] . '.'));
    }
    // Check the format for the supplied twitter part and input, if not pass, then set error message.
    elseif (!preg_match($twitter_parts[$twitter_part]['reg_expression'], $input_to_validate)) {

      // Set the error message.
      $form_state->setErrorByName('settings][block_form][' . $twitter_parts[$twitter_part]['field_name'], t('There was an error in the Twitter ' . $twitter_parts[$twitter_part]['name'] . '.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'alterForm',
    ];
  }
}
