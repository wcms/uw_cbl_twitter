<?php

namespace Drupal\uw_cbl_twitter\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblTwitterPreprocessBlock.
 */
class UwCblTwitterPreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with facebook and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_twitter')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content')['#block_content'];

      // Get the feed type.
      $feed_type = $block->get('field_uw_tw_feed_type')->getValue()[0]['value'];

      // Get the type or twitter feed.
      $feed_type = $block->field_uw_tw_feed_type->value;

      // If we are on an embedded tweet, we need to get the tweet code.
      if ($feed_type == 'embed-tweet') {

        // Get the value from the field.
        $tweet_code_raw = $block->field_uw_tw_tweet_code->value;

        // Get out the tweet code from the embedded code.
        preg_match('/.*twitter\.com\/([^"\/]+\/status\/[0-9]+)(?:\?((?!:"|&quot;).)*)*(?:"|&quot;|\/?$)/', $tweet_code_raw, $matches);

        // If there is a match set the tweet_code variable.
        if (isset($matches[1])) {

          // Set the tweet code variable.
          $tweet_code = $matches[1];
        }
      }
      // If we are not on embedded tweet, get the settings out.
      else {

        // We need a username on the rest, so grab it.
        $username = $block->field_uw_tw_username->value;

        // If we are on a timeline or moment, we need the url fragment and timeline name.
        if ($feed_type == 'timeline' || $feed_type == 'moment') {

          // Need the URL fragment for both timeline and moment, so grab it.
          $url_fragment = $block->field_uw_tw_url_fragment->value;

          // If we are on a timeline, get the timeline name.
          if ($feed_type == 'timeline') {

            // Get the the timeline name.
            $timeline_name = $block->field_uw_tw_timeline_name->value;
          }
        }

        // If we are on a list, we need the list name.
        if ($feed_type == 'list') {

          // Get the list name.
          $list_name = $block->field_uw_tw_list_name->value;
        }
      }

      // Set the variables required for twitter.
      $twitter = [
        'feed_type' => $feed_type,
        'username' => isset($username) ? $username : '',
        'tweet_code' => isset($tweet_code) ? $tweet_code : '',
        'list_name' => isset($list_name) ? $list_name : '',
        'url_fragment' => isset($url_fragment) ? $url_fragment : '',
        'timeline_name' => isset($timeline_name) ? $timeline_name : '',
      ];

      // Set the settings for twitter in variables.
      $variables->set('twitter', $twitter);
    }
  }
}
